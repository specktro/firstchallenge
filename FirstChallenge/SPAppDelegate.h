//
//  SPAppDelegate.h
//  FirstChallenge
//
//  Created by specktro on 04/02/14.
//  Copyright (c) 2014 specktro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
//Testing
@end
